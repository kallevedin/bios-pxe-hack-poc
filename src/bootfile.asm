;------------------------------------------------------------------------------
;
; bootfile is loaded by bios to 07C0:0000
;
;------------------------------------------------------------------------------



[org 0]				
[bits 16]

	jmp 0x07C0:start	; make sure our program starts at offset 0x0000
start:
	mov ax, 0x07c0
	mov ds, ax		; MBR is loaded by bios to 07C0:0000

	mov ax, es
	mov [PXENstruct], ax
	mov [PXENstruct+2], bx	; save the ES:BX pointer (that points to the PXENV+ struct)

	cli			; no int's when there is no stack!
	mov ax, 0x9000
	mov ss, ax		; put the stack to 9000:ffff = 9ffff
	mov sp, 0xffff		; (remember: it grows downwards)
	sti

	mov ax, [PXENstruct]
	mov es, ax
	mov bx, [PXENstruct+2]
	mov eax, "PXEN"
	cmp [es:bx], eax
	jne .fail

.l1
	mov bx, [PXENstruct+2]
	mov ax, [PXENstruct]
	mov es, ax
	mov ax, [ES:BX+6]
	cmp ax, 0x201			; PXE version >= 2.1?
	jae .PXE
	jb .PXENV

.PXE:					; Yes, PXE version >= 2.1
	mov bx, [PXENstruct+2]
	mov ax, [PXENstruct]
	mov es, ax
	mov ax, [ES:BX+0x28]
	mov bx, ax
	mov eax, [ES:BX]
	cmp eax, "!PXE"
	jne .fail

	; save the pointer to !PXE
	mov ax, es
	mov [PXEPointer], ax
	mov [PXEPointer+2], bx

	call udp_client
	call boot_hdd
		
.fail	
.PXENV:

	jmp $

%include "pxe-udp-client.asm"
%include "boot-hdd.asm"

;-----------------------------------------------

PXENstruct	dw	0,0
PXEPointer	dw	0,0

        times 1022-($-$$) db 0		; aka 8KB large boot file
        dw 0xAA55			; 8190+2 = 8kB
