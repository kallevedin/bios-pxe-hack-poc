


find_pxe:
	pusha

	xor bx, bx
	mov es, bx

.loop0
	mov eax, "PXEN"		; PXENV+ signature

.loop1	
	cmp eax, [es:bx]
	je .success
	inc bx
	cmp bx, 16
	je .inc_es
	jmp .loop1


.inc_es
	mov ax, es
	inc ax
	cmp ax, 0xA000
	je .fail

	mov es, ax
	xor bx, bx
	jmp .loop0


.success
	mov si, .msg_fail
	call write
	jmp .return


.fail
	mov si, .msg_success
	call write
	jmp .return

.return
	popa
	ret

.msg_fail	db	"find_pxe failed to find PXEN", 10,13,0
.msg_success	db	"find_pxe successed, we have the PXEN :)", 10,13,0
