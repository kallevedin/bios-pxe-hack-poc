
boot_hdd:
	  mov     ax, 0x07c0
	  mov     es, ax
          xor     bx, bx

          mov     di, 5                               ; 5 tries
          mov     ah, 0x02                            ; BIOS read sector
          mov     al, 0x01                            ; read one sector
          mov     ch, 0x00                            ; track
          mov     cl, 0x01                            ; sector
          mov     dh, 0x00                            ; head
          mov     dl, 0x80                            ; drive (0 = hda)
.loop1:   
	  int     0x13                                ; invoke BIOS
          jnc     .success                            ; test for read error

          xor     ax, ax                              ; BIOS reset disk
          int     0x13                                ; invoke BIOS
          dec     di                                  ; decrement error counter
          jnz     .loop1
          int     0x18                                ; display an error message (or load the BASIC program lol ;)
.success: 
	  jmp 0x07c0:0x0000          
