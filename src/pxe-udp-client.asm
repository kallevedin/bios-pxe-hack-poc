%include "constants.inc"

udp_client:
	call .get_pointer
		; -- make sure that the pointer is correct --
		mov eax, "!PXE"
		cmp [es:bx], eax
		jne .fail

	call .open_udp_socket
		cmp ax, 0
		jne .xxxfail
	call .create_packet

	mov cx, 3
.flood_network:
	call .write_to_udp_socket
		cmp ax, 0
		jne .xxxfail

	dec cx
	jnz .flood_network
	ret

.xxxfail:
	jmp $

.write_ok:
	ret

.message	db	"Ok.",10,13,0
.msg_open	db	"Opening UDP socket...",0
.msg_write	db	"Sending one UDP packet...",0
.msg_fail	db	"FAILED.",10,13,"Error code 0x",0
.msg_ok		db	"OK.",10,13,0

.fail
	push ax
	mov ax, cs
	mov ds, ax
	pop ax
	jmp $

.get_pointer:
	push	ax
	mov	ax, [PXEPointer]
	mov	es, ax
	mov	bx, [PXEPointer+2]
	pop	ax
	ret

.open_udp_socket:
	push cs
	mov ax, t_PXENV_UDP_OPEN
	push ax
	push PXENV_UDP_OPEN

	; call the function
	call .get_pointer
	add bx, 0x10
	call far [es:bx]
	; clean up the stack (as defined in the spec)
	add sp, 6		; caller cleans up
	ret

.get_ip_from_dhcp:
	pusha

	; --- skapar buffer att kopiera paketet till ---
	mov ax, outside
	mov [t_PXENV_GET_CACHED_INFO.buffer_off], ax
	mov ax, cs
	mov [t_PXENV_GET_CACHED_INFO.buffer_seg], ax
	mov ax, 512
	mov [t_PXENV_GET_CACHED_INFO.buffer_size], ax

	; --- Hämtar data från NIC BIOS (gör funktionsanropet) ---
	push cs
	push t_PXENV_GET_CACHED_INFO
	push 0x0071 			; = PXENV_GET_CACHED_INFO
	call .get_pointer
	add bx, 0x10
	call [es:bx]
	add sp, 6		; rensar upp stacken
		cmp ax, 0
		jne .fail

	; sätter vårt IP nummer till det DHCP gav oss...
	mov eax, [outside+16]
	mov [t_PXENV_UDP_OPEN.src_ip], eax

	popa
	ret

.create_packet:
	push ax
	mov ax, cs
	mov [t_PXENV_UDP_WRITE.ptr_packet_seg], ax
	mov ax, udp_packet
	mov [t_PXENV_UDP_WRITE.ptr_packet_off], ax
	pop ax
	ret

.write_to_udp_socket:
	push cs
	mov ax, t_PXENV_UDP_WRITE
	push ax
	push PXENV_UDP_WRITE
	call .get_pointer
	call far [es:bx+0x10]
	add sp, 6		; caller cleans up
	ret

t_PXENV_UDP_OPEN
	.status		dw	0
	.src_ip		db	172,16,0,20

t_PXENV_UDP_WRITE
	.status		dw	0
	.dst_ip		db	172,16,0,1
	.gw		db	0, 0, 0, 0		; is set automagic 
	.src_port	dw	0x3500
	.dst_port	dw	0x3500
	.packet_size	dw	PACKET_SIZE
	.ptr_packet_off	dw	0		; offset
	.ptr_packet_seg	dw	0		; segment

t_PXENV_GET_CACHED_INFO
	.status		dw	0
	.packet_type	dw	2
	.buffer_size	dw	0
	.buffer_off	dw	0
	.buffer_seg	dw	0
	.buffer_limit	dw	0

udp_packet
	.message	db	"Hello there ;)",0
			dd	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	PACKET_SIZE	equ	($-udp_packet)

outside:
