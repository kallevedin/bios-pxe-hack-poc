
The Z7 project
==============

A study & PoC of the PXE network stack - in particular if it could be used to send UDP/TCP messages *BEFORE* the operating system had been loaded.

It turns out that it is totally doable. Its just a pain in the ass to read the documentation and figure out how to do stuff. We took some hacky short-cuts.

Z7 will, if installed on a medium that appears first in the boot sequence send a single UDP message to a configurable IP address and then continue booting (int 0x19) to the next medium. This PoC can be installed on a TFTP server and PXE-booted, or loaded off a floppy disk. Booting, running the program and then jumping to the next bootable medium happens really fast and looks quite normal, so I guess most people would not notice it.

:)
